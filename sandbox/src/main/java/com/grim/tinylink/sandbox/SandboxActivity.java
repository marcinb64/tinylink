/*
 * Copyright (c) 2015 Marcin Blazejewski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.grim.tinylink.sandbox;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.grim.tinylink.CommLink;
import com.grim.tinylink.CommNode;
import com.grim.tinylink.DataListener;
import com.grim.tinylink.LinkEventListener;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public abstract class SandboxActivity extends Activity {

    protected static final String TAG = "Sandbox";

    private TextView console;
    private EditText edit;

    private CommNode commNode;
    private CommLink commLink;

    private MenuItem connectionAction;

    private static class TestObject implements Serializable {
        public String message;
        public int timestamp;
        private static int counter = 0;

        public TestObject(String message) {
            this.message = message;
            this.timestamp = counter++;
        }
    }

    private View.OnClickListener txListener = new View.OnClickListener() {
        @SuppressLint("SetTextI18n")
        @SuppressWarnings("ConstantConditions")
        @Override
        public void onClick(View view) {
            if (!isConnected())
                return;

            String txt = edit.getText().toString();
            if (txt.trim().length() == 0)
                return;

            consolePrint(txt);

            try {
                commLink.send(new TestObject(txt));
            } catch (IOException e) {
                Log.e(TAG, "Tx: " + e);
            }
        }
    };

    private LinkEventListener eventListener = new LinkEventListener() {
        @Override
        public void onNewLink(final CommLink link) {
            commLink = link;
            link.setDataListener(linkDataListener);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    enableDisconnectControls();
                    consolePrint(getString(R.string.msg_connected, link));
                }
            });
        }

        @Override
        public void onLinkDisconnected(final CommLink link) {
            commLink = null;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    enableConnectionControls();

                    consolePrint(getString(R.string.msg_disconnected, link));
                }
            });
        }
    };

    private DataListener linkDataListener = new DataListener() {
        @Override
        public void onObjectReceived(CommLink link, Object data) {
            //final String msg = (String) data;
            TestObject obj = (TestObject) data;
            final String msg = obj.message + " @" + obj.timestamp;
            Log.v(TAG, "Rx(" + link + ") " + msg);

            runOnUiThread(new Runnable() {
                @SuppressLint("SetTextI18n")
                @Override
                public void run() {
                    consolePrint(msg.trim());
                }
            });
        }
    };

    private DataListener dataListener = new DataListener() {
        @Override
        public void onObjectReceived(CommLink link, Object data) {
            //final String msg = (String) data;
            TestObject obj = (TestObject) data;
            final String msg = obj.message + " @" + obj.timestamp;
            Log.v(TAG, "Rx(" + link + ") " + msg);

            runOnUiThread(new Runnable() {
                @SuppressLint("SetTextI18n")
                @Override
                public void run() {
                    consolePrint(msg.trim());
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit = (EditText) findViewById(R.id.command_entry);
        edit.setText(Build.MODEL);

        findViewById(R.id.tx).setOnClickListener(txListener);

        console = (TextView) findViewById(R.id.console);
        console.setMovementMethod(new ScrollingMovementMethod());
        console.setText("");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopServer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        disconnect();
    }

    protected abstract void askForConnectionAddress();

    protected boolean isConnected() {
        return commLink != null;
    }

    public void disconnect() {
        if (commLink == null)
            return;

        commLink.disconnect();
        commLink = null;

        enableConnectionControls();
    }

    protected void startServer() {
        Log.i(TAG, "start server");

        try {
            commNode = createCommNode();
            commNode.setEventListener(eventListener);
            commNode.setDataListener(dataListener);
        } catch (IOException e) {
            Log.e(TAG, "init comm node", e);
            Toast.makeText(this, "Error setting up comm node: " + e, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    protected abstract CommNode createCommNode() throws IOException;

    private void stopServer() {
        Log.i(TAG, "stop server");

        if (commNode != null) {
            commNode.kill();
            commNode = null;
        }
    }

    public void unpair(BluetoothDevice device) {
        try {
            Method m = device.getClass().getMethod("removeBond", (Class[]) null);
            m.invoke(device, (Object[]) null);
            consolePrint("Unpaired " + device.getAddress());
        } catch (Exception e) {
            Log.e(TAG, "Unpair: " + e);
        }
    }

    @SuppressLint("SetTextI18n")
    public void consolePrint(String message) {
        console.setText(console.getText() + message + "\n");
    }

    protected class ConnectTask extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            disableConnectionControls();
            consolePrint(getString(R.string.msg_connecting));
        }

        @Override
        protected Void doInBackground(String... addresses) {
            try {
                commLink = commNode.linkTo(addresses[0]);
            } catch (IOException e) {
                Log.e(TAG, "connect: " + e);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (!isConnected())
                consolePrint(getString(R.string.msg_connection_failed));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        connectionAction = menu.findItem(R.id.action_connection);
        enableConnectionControls();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_connection:
                if (!isConnected()) {
                    askForConnectionAddress();
                } else {
                    disconnect();
                }

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void enableConnectionControls() {
        if (connectionAction != null) {
            connectionAction.setEnabled(true);
            connectionAction.setTitle("Connect");
            connectionAction.setIcon(android.R.drawable.ic_media_play);
        }
    }

    protected void enableDisconnectControls() {
        if (connectionAction != null) {
            connectionAction.setEnabled(true);
            connectionAction.setTitle("Disconnect");
            connectionAction.setIcon(android.R.drawable.ic_media_pause);
        }
    }

    protected void disableConnectionControls() {
        if (connectionAction != null) {
            connectionAction.setEnabled(false);
        }
    }
}
