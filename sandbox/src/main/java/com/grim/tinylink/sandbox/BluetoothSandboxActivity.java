/*
 * Copyright (c) 2015 Marcin Blazejewski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.grim.tinylink.sandbox;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.grim.tinylink.CommNode;
import com.grim.tinylink.bluetooth.BluetoothNode;

import java.io.IOException;
import java.util.UUID;

public class BluetoothSandboxActivity extends SandboxActivity {

    private static final UUID SANDBOX_UUID =
            UUID.fromString("e06ebccc-8d44-11e5-8994-feff819cdc9f");

    protected static final int REQUEST_CONNECT = 100;
    protected static final int REQUEST_UNPAIR = 102;
    protected static final int REQUEST_ENABLE_BT = 200;

    protected BluetoothAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = BluetoothAdapter.getDefaultAdapter();
        if (adapter.isEnabled())
            startServer();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!adapter.isEnabled()) {
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                    REQUEST_ENABLE_BT);
            disableConnectionControls();
        } else {
            enableConnectionControls();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_unpair:
                Intent intent = new Intent(this, DeviceSelectorActivity.class);
                startActivityForResult(intent, REQUEST_UNPAIR);
                return true;

            case R.id.action_reset_bt:
                Log.i(TAG, "Disable BT");
                adapter.disable();
                try { Thread.sleep(1000); } catch (InterruptedException ignore) {}
                Log.i(TAG, "Enable BT");
                adapter.enable();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected CommNode createCommNode() throws IOException {
        return new BluetoothNode("sandbox", SANDBOX_UUID);
    }


    @Override
    protected void askForConnectionAddress() {
        startActivityForResult(new Intent(this, DeviceSelectorActivity.class),
                REQUEST_CONNECT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CONNECT && resultCode == RESULT_OK) {
            BluetoothDevice device = data.getParcelableExtra(DeviceSelectorActivity.EXTRA_DEVICE);

            disconnect();
            new ConnectTask().execute(device.getAddress());
        }
        else if (requestCode == REQUEST_UNPAIR && resultCode == RESULT_OK) {
            BluetoothDevice device = data.getParcelableExtra(DeviceSelectorActivity.EXTRA_DEVICE);
            unpair(device);
        }
        else if (requestCode == REQUEST_ENABLE_BT && resultCode == RESULT_OK) {
            enableConnectionControls();
            startServer();
        }
    }


}