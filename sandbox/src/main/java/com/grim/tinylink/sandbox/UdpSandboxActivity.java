/*
 * Copyright (c) 2015 Marcin Blazejewski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.grim.tinylink.sandbox;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.EditText;

import com.grim.tinylink.CommNode;
import com.grim.tinylink.udp.UdpNode;

import java.io.IOException;

public class UdpSandboxActivity extends SandboxActivity {

    public static final int SANDBOX_PORT = 9007;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startServer();
    }

    @Override
    protected void askForConnectionAddress() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter target IP");
        final EditText input = new EditText(this);
        builder.setView(input);
        builder.setPositiveButton("Connect", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String ip = input.getText().toString();
                new ConnectTask().execute(ip + ":" + SANDBOX_PORT);
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

    @Override
    protected CommNode createCommNode() throws IOException {
        return new UdpNode(SANDBOX_PORT);
    }
}
