/*
 * Copyright (c) 2015 Marcin Blazejewski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.grim.tinylink.sandbox;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Set;


public class DeviceSelectorActivity extends Activity {

    public static final String EXTRA_DEVICE = "device";

    private static final String TAG = "DeviceSelector";

    private BluetoothAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_selector);

        adapter = BluetoothAdapter.getDefaultAdapter();
        deviceListAdapter = new DeviceListAdapter(this);

        ListView deviceList = (ListView) findViewById(R.id.selector_device_list);
        deviceList.setAdapter(deviceListAdapter);
        deviceList.setOnItemClickListener(deviceClickListener);
        //deviceList.setOnItemLongClickListener(deviceLongClickListener);

    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        registerReceiver(deviceListAdapter.deviceDiscoveredReceiver, filter);

        //filter = new IntentFilter(BluetoothDevice.ACTION_UUID);
        //registerReceiver(deviceListAdapter.serviceDiscoveredReceiver, filter);

        //adapter.startDiscovery();
        deviceListAdapter.addDevices(adapter.getBondedDevices());
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(deviceListAdapter.deviceDiscoveredReceiver);
        adapter.cancelDiscovery();
    }

    private DeviceListAdapter deviceListAdapter;

    private class DeviceListAdapter extends ArrayAdapter<BluetoothDevice> {

        public DeviceListAdapter(Context context) {
            super(context, R.layout.device_list_item);
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.device_list_item, null);
                if (view == null) return null;
            }

            BluetoothDevice dev = getItem(i);
            TextView nameView = (TextView) view.findViewById(R.id.devitem_name);
            TextView addressView = (TextView) view.findViewById(R.id.devitem_address);

            if (dev.getName() == null) {
                nameView.setText(dev.getAddress());
                //addressView.setText(describeUuids(dev));
            }
            else {
                nameView.setText(dev.getName());
                //addressView.setText(dev.getAddress() + "\n" + describeUuids(dev));
                addressView.setText(dev.getAddress());
            }

            return view;
        }

        /*
        private String describeUuids(BluetoothDevice dev) {
            String description = "";

            if (dev.getUuids() != null) {
                for (ParcelUuid uuid : dev.getUuids())
                    description += "  " + uuid + "\n";
            }

            return description;
        }
        */

        public boolean containsDevice(BluetoothDevice dev) {
            for (int i = 0; i < getCount(); i++)
                if (dev.equals(getItem(i)))
                    return true;

            return false;
        }

        public void addDevices(Set<BluetoothDevice> devices) {
            for (BluetoothDevice dev : devices) {
                Log.d(TAG, "bonded device: " + dev);
                if (!containsDevice(dev))
                    add(dev);
            }

            notifyDataSetChanged();
        }

        private BroadcastReceiver deviceDiscoveredReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (!containsDevice(device))
                    add(device);

                Log.d(TAG, "Device discovered " + device);
                notifyDataSetChanged();
            }
        };

        /*
        private BroadcastReceiver serviceDiscoveredReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                BluetoothDevice deviceExtra = intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                Parcelable[] uuidExtra = intent.getParcelableArrayExtra("android.bluetooth.device.extra.UUID");

                if (deviceExtra == null) return;
                if (uuidExtra == null) return;

                Log.d(TAG, "SDP " + deviceExtra.getAddress() + " : " + deviceExtra.getName());
                for (Parcelable uuidParcel : uuidExtra) {
                    String uuid = uuidParcel.toString();
                    Log.d(TAG, "  service " + uuid);
                }

                BluetoothClass bc = deviceExtra.getBluetoothClass();
                Log.d(TAG, "class object " + bc);
                Log.d(TAG, "class major " + bc.getMajorDeviceClass() + "device class " + bc.getDeviceClass());

                notifyDataSetChanged();
            }
        };
        */

    }

    private AdapterView.OnItemClickListener deviceClickListener =
            new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent dataIntent = new Intent();
                    BluetoothDevice device = deviceListAdapter.getItem(i);
                    dataIntent.putExtra(EXTRA_DEVICE, device);
                    setResult(RESULT_OK, dataIntent);
                    finish();
                }
            };


    /*
    private AdapterView.OnItemLongClickListener deviceLongClickListener =
            new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                    BluetoothDevice device = deviceListAdapter.getItem(i);
                    device.fetchUuidsWithSdp();

                    return true;
                }
            };
    */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.device_selector, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_discover_devices:
                adapter.startDiscovery();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

}
