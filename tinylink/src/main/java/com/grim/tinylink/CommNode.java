/*
 * Copyright (c) 2015 Marcin Blazejewski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.grim.tinylink;

import java.io.IOException;

public abstract class CommNode {
    private LinkEventListener eventListener = LinkEventListener.nullListener;
    private DataListener dataListener = DataListener.nullListener;

    public abstract void kill();
    public abstract CommLink linkTo(String address) throws IOException;


    public void setEventListener(LinkEventListener listener) {
        if (listener != null)
            eventListener = listener;
        else
            eventListener = LinkEventListener.nullListener;
    }

    public void setDataListener(DataListener listener) {
        if (listener != null)
            dataListener = listener;
        else
            dataListener = DataListener.nullListener;
    }

    protected void notifyAboutNewLink(CommLink link) {
        eventListener.onNewLink(link);
    }

    protected void notifyLinkDisconnected(CommLink link) {
        eventListener.onLinkDisconnected(link);
    }

    protected void notifyObjectReceived(CommLink link, Object object) {
        dataListener.onObjectReceived(link, object);
    }

}
