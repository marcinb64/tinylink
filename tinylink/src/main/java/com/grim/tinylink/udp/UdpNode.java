/*
 * Copyright (c) 2015 Marcin Blazejewski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.grim.tinylink.udp;

import android.util.Log;

import com.grim.tinylink.CommNode;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;

public class UdpNode extends CommNode {
    private static final String TAG = "UdpNode";
    private static final int RX_TIMEOUT = 1000;
    private static final int RX_BUFFER_SIZE = 4096;

    private Thread rxThread;
    private TransmitterThread txThread;
    private boolean isAlive = true;

    private final DatagramSocket socket;
    private final byte rxBuffer[] = new byte[RX_BUFFER_SIZE];

    private HashMap<InetSocketAddress, UdpLink> allLinks = new HashMap<>();


    public UdpNode(int port) throws IOException {
        socket = new DatagramSocket(port);
        socket.setSoTimeout(RX_TIMEOUT);

        rxThread = new Thread(rxTask);
        rxThread.start();

        txThread = new TransmitterThread();
        txThread.start();

        Log.d(TAG, "Listening on " + socket.getLocalPort());
    }

    @Override
    public void kill() {
        isAlive = false;
        rxThread.interrupt();

        txThread.kill();
    }

    @SuppressWarnings("FieldCanBeLocal")
    private Runnable rxTask = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "rx alive");
            while (isAlive) {
                try {
                    receiveAndProcessData();
                }
                catch (SocketTimeoutException ignore) {
                }
                catch (IOException e) {
                    Log.e(TAG, "rx: " + e);
                }
            }
            socket.close();
            Log.d(TAG, "rx dead");
        }
    };

    private void receiveAndProcessData() throws IOException {
        DatagramPacket packet = new DatagramPacket(rxBuffer, rxBuffer.length);
        socket.receive(packet);

        Object receivedObject = extractObjectFromPacket(packet);
        InetSocketAddress sourceAddress = (InetSocketAddress) packet.getSocketAddress();

        UdpLink link = allLinks.get(sourceAddress);
        if (link == null) {
            link = new UdpLink(this, sourceAddress);
            addNewLink(link);
            link.onObjectReceived(link, receivedObject);
        }
        else {
            notifyObjectReceived(link, receivedObject);
        }
    }

    private Object extractObjectFromPacket(DatagramPacket packet) throws IOException {
        ObjectInputStream inputStream = new ObjectInputStream(
                new ByteArrayInputStream(packet.getData(), packet.getOffset(), packet.getLength()));

        try {
            return inputStream.readObject();
        } catch (ClassNotFoundException e) {
            throw new IOException(e.getMessage());
        }
    }

    ByteArrayOutputStream txBuffer = new ByteArrayOutputStream();

    void sendTo(SocketAddress address, Object message) throws IOException {
        txBuffer.reset();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(txBuffer);

        objectOutputStream.writeObject(message);
        byte[] data = txBuffer.toByteArray();
        final DatagramPacket packet = new DatagramPacket(data, data.length, address);

        txThread.handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    socket.send(packet);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public UdpLink linkTo(String addressString) throws UnknownHostException {
        InetSocketAddress address = parseAddress(addressString);

        UdpLink link = allLinks.get(address);
        if (link == null) {
            link = new UdpLink(this, address);
            addNewLink(link);
        }

        return link;
    }

    private InetSocketAddress parseAddress(String address) {
        String tokens[] = address.split(":");
        if (tokens.length < 2)
            throw new IllegalArgumentException("not a valid address");

        int port;
        try {
            port = Integer.parseInt(tokens[1]);
        }
        catch (NumberFormatException e) {
            throw new IllegalArgumentException("invalid port number");
        }

        return new InetSocketAddress(tokens[0], port);
    }

    private void addNewLink(UdpLink link) {
        allLinks.put(link.getRemoteAddress(), link);
        notifyAboutNewLink(link);
    }

    public void removeLink(UdpLink link) {
        InetSocketAddress address = link.getRemoteAddress();
        if (!allLinks.containsKey(address))
            return;

        allLinks.remove(address);
        notifyLinkDisconnected(link);
    }

}
