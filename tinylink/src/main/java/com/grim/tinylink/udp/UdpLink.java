/*
 * Copyright (c) 2015 Marcin Blazejewski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.grim.tinylink.udp;

import com.grim.tinylink.CommLink;

import java.io.IOException;
import java.net.InetSocketAddress;

public class UdpLink extends CommLink {
    private final InetSocketAddress remoteAddress;

    UdpLink(UdpNode node, InetSocketAddress remoteAddress) {
        super(node);
        this.remoteAddress = remoteAddress;
    }

    @Override
    public void disconnect() {
        ((UdpNode)commNode).removeLink(this);
    }

    @Override
    public void send(Object object) throws IOException {
        ((UdpNode)commNode).sendTo(remoteAddress, object);
    }

    public InetSocketAddress getRemoteAddress() {
        return remoteAddress;
    }

    @Override
    public String toString() {
        return remoteAddress.toString();
    }

    void onObjectReceived(CommLink link, Object object) {
        notifyObjectReceived(link, object);
    }
}
