/*
 * Copyright (c) 2015 Marcin Blazejewski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.grim.tinylink.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.grim.tinylink.CommLink;
import com.grim.tinylink.CommNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

@SuppressWarnings("ResourceType")
public class BluetoothNode extends CommNode {

    @SuppressWarnings("unused")
    public final static UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static final String TAG = "BluetoothNode";
    private final UUID serviceUUID;

    private BluetoothServerSocket serverSocket;
    private boolean isAlive = true;

    private ArrayList<BluetoothLink> linkList = new ArrayList<>();


    public BluetoothNode(String serviceName, UUID serviceUUID) throws IOException {
        this.serviceUUID = serviceUUID;
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        serverSocket = adapter.listenUsingRfcommWithServiceRecord(serviceName, serviceUUID);

        new Thread(new AcceptRunnable()).start();
    }

    @Override
    public void kill() {
        killServerSocket();
        killLinks();
    }

    private void killServerSocket() {
        isAlive = false;
        try {
            if (serverSocket != null)
                serverSocket.close();
        } catch (IOException ignore) {
        }

        serverSocket = null;
    }

    private void killLinks() {
        for (BluetoothLink link : linkList)
            link.disconnect();
    }

    private class AcceptRunnable implements Runnable {

        @Override
        public void run() {
            Log.d(TAG, "Server alive");

            while (isAlive) {
                try {
                    BluetoothSocket socket = serverSocket.accept();

                    Log.d(TAG, "New connection from " + socket.getRemoteDevice());
                    BluetoothLink link = new BluetoothLink(BluetoothNode.this, socket);
                    linkList.add(link);
                    notifyAboutNewLink(link);
                } catch (IOException e) {
                    if (!isAlive)
                        break;

                    Log.e(TAG, "accept client: " + e);
                }
            }

            Log.d(TAG, "Server dead");
        }
    }

    @Override
    public CommLink linkTo(String address) throws IOException {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        BluetoothDevice device = adapter.getRemoteDevice(address);
        BluetoothLink link;

        adapter.cancelDiscovery();

        Log.d(TAG, "Connecting to " + device);
        BluetoothSocket socket = device.createRfcommSocketToServiceRecord(serviceUUID);
        socket.connect();
        Log.d(TAG, "Connected.");

        link = new BluetoothLink(this, socket);
        linkList.add(link);
        notifyAboutNewLink(link);

        return link;
    }

    void removeLink(BluetoothLink link) {
        if (!linkList.contains(link))
            return;

        linkList.remove(link);
        notifyLinkDisconnected(link);
    }
}
