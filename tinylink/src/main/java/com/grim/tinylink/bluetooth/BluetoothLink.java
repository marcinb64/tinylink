/*
 * Copyright (c) 2015 Marcin Blazejewski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.grim.tinylink.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import com.grim.tinylink.CommLink;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

@SuppressWarnings("ResourceType")
public class BluetoothLink extends CommLink implements Runnable {
    private static final String TAG = "BluetoothLink";

    private boolean isAlive = true;
    private BluetoothDevice remoteDevice;
    private ObjectInputStream inputStream;
    private ObjectOutputStream outputStream;

    BluetoothLink(BluetoothNode node, BluetoothSocket socket) throws IOException {
        super(node);
        this.outputStream = new ObjectOutputStream(socket.getOutputStream());
        this.inputStream = new ObjectInputStream(socket.getInputStream());
        this.remoteDevice = socket.getRemoteDevice();

        new Thread(this).start();
    }


    @Override
    public void disconnect() {
        isAlive = false;
        try {
            inputStream.close();
        } catch (IOException ignore) {
        }
    }

    @Override
    public void send(Object object) throws IOException {
        outputStream.writeObject(object);
    }

    @Override
    public void run() {
        Log.d(TAG, "alive");

        while (isAlive) {
            try {
                Object object = inputStream.readObject();
                notifyObjectReceived(this, object);
            } catch (SocketClosedException e) {
                Log.i(TAG, "connection terminated by peer");
                isAlive = false;
            } catch (IOException e) {
                if (isAlive)
                    Log.e(TAG, "Rx: " + e);
                isAlive = false;
            } catch (ClassNotFoundException e) {
                Log.w(TAG, "received unknown object: " + e);
            }
        }

        ((BluetoothNode)commNode).removeLink(this);
        Log.d(TAG, "dead");
    }

    @SuppressWarnings("UnusedDeclaration")
    protected void readAll(byte buffer[], int offset, int count) throws IOException {
        int n;

        while (count > 0) {
            try {
                n = inputStream.read(buffer, offset, count);
            } catch (IOException e) {
                if (e.getMessage().contains("socket closed"))
                    throw new SocketClosedException();
                throw e;
            }

            if (n <= 0)
                throw new SocketClosedException();

            offset += n;
            count -= n;
        }
    }

    /*public void sendData(byte data[], BluetoothLink node) {
        transmitter.sendObject(data, node);
    }*/

    @Override
    public String toString() {
        if (remoteDevice == null)
            return "unknown";

        if (remoteDevice.getName() != null)
            return remoteDevice.getName();

        return remoteDevice.getAddress();
    }
}
