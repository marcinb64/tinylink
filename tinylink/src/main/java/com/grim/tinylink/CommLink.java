/*
 * Copyright (c) 2015 Marcin Blazejewski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.grim.tinylink;

import java.io.IOException;

public abstract class CommLink {
    protected final CommNode commNode;
    private DataListener dataListener = null;

    protected CommLink(CommNode commNode) {
        this.commNode = commNode;
    }

    public void setDataListener(DataListener listener) {
        if (listener != null)
            dataListener = listener;
        else
            dataListener = null;
    }

    protected void notifyObjectReceived(CommLink link, Object object) {
        if (dataListener != null)
            dataListener.onObjectReceived(link, object);
        else
            commNode.notifyObjectReceived(link, object);
    }

    public abstract void disconnect();
    public abstract void send(Object object) throws IOException;

}
